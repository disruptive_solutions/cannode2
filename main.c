/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using MPLAB(c) Code Configurator

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.15
        Device            :  PIC18F25K80
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40
*/

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/
#include <stdlib.h>
#include "mcc_generated_files/mcc.h"
#include "lcd.h"
#include "myECAN.h"

//#define DATA_PORT      		PORTB
// #define TRIS_DATA_PORT 		TRISB
//
// #define RS_PIN   LATCbits.LATC4   		/* PORT for RS */
// #define TRIS_RS  TRISCbits.TRISC4   	/* TRIS for RS */
//
// #define E_PIN    LATCbits.LATC5  		/* PORT for E  */
// #define TRIS_E   TRISCbits.TRISC5  

unsigned short dt;

void DelayFor18TCY(void)
 {
 Delay10TCYx(0x2); //delays 20 cycles

 return;
 }
 /*****/
 void DelayPORXLCD(void)   // minimum 15ms
 {
 Delay100TCYx(0xA0);   // 100TCY * 160
 return;
 }
 /*****/
 void DelayXLCD(void)     // minimum 5ms
 {
 Delay100TCYx(0x36);      // 100TCY * 54
 return;
 }
 
void main(){
    unsigned char read_flag;
    uCAN_MSG *tempCanMsg;

    SYSTEM_Initialize();
    LCD_Initialize();
    
    Delay10KTCYx(250);
    LCDGoto(1,0);
    LCDPutStr("Disruptive");
    LCDGoto(1,1);
    LCDPutStr("Solutions");
    
    Delay10KTCYx(250);

    //while(BusyXLCD());
    
    Delay10KTCYx(250);
    LCDPutCmd(LCD_CLEAR); 

    Delay10KTCYx(250);
    
    __delay_ms(25);
    unsigned char temp;
    // Enter CAN module into config mode
    CANCON = 0x80;
    while(!(CANCON & 0x80));
    
    // Enter CAN module into Mode 1
    ECANCON = 0x40;
    
    // Initialize CAN Timing
    //  100 Kbps @ 25Mhz
    //BRGCON1 = 0x44; //0100 0100
    //BRGCON2 = 0xBF; //1011 1111
    //BRGCON3 = 0x07; //0000 0111
    
    //  250 Kbps @ 25Mhz
    //BRGCON1 = 0x04; //0000 0011
    //BRGCON2 = 0x92; //1001 0010
    //BRGCON3 = 0x82; //0000 0010

    //  100 Kbps @ 64MHz
//    BRGCON1 = 0x1F; //0001 1111  
//    BRGCON2 = 0xA0; //1010 0000
//    BRGCON3 = 0x02; //0000 0010 

    // Setup Programmable buffers
    //  B0 is a receive buffer AND B2,B3,B4,B5 are Transmit buffers
   // BSEL0 = 0xF8;   //1111 10--
    
    // Initialize Receive Masks
    //  The first mask is used that accepts all SIDs and no EIDs
    //  The second mask is used to ignore all SIDs and EIDs
    RXM0EIDH = 0x00;    // 0's for EID and 1's for SID
    RXM0EIDL = 0x00;
    RXM0SIDH = 0xFF;
    RXM0SIDL = 0xFF;

    RXM1EIDH = 0x00;    // 0's for EID and SID
    RXM1EIDL = 0x00;
    RXM1SIDH = 0x00;
    RXM1SIDL = 0x00;
    
    // Enable Filters
    //  Only using first three filters, so the rest can be disabled.
    RXFCON0 = 0x07;     //Enable Filters 0,1,2
    RXFCON1 = 0x00;     //Disable all others
    
    // Assign Filters to Masks
    //  Only one mask is used for three filters
    MSEL0 = 0xC0;     //Assign Filters 0-2 to Mask 0 and 3 to Mask 1
    MSEL1 = 0xFF;     //Assign Filters 4-7 to Mask 1
    MSEL2 = 0xFF;     //Assign Filters 8-11 to Mask 1
    MSEL3 = 0xFF;     //Assign Filters 12-15 to Mask 1
    
    // Assign Filters to Buffers
    //  Have the first buffer only accept the first filter, the second buffer accept
    //  the second filter, and the third buffer accept the third filter.
    RXFBCON0 = 0x10;     //Assign Filter 0 to RXB0, and Filter 1 to RXB1
    RXFBCON1 = 0xF2;     //Assign Filter 2 to B0
    
    RXFBCON2 = 0xFF;     //Assign the rest of the buffers with no filter
    RXFBCON3 = 0xFF;
    RXFBCON4 = 0xFF;
    RXFBCON5 = 0xFF;
    RXFBCON6 = 0xFF;
    RXFBCON7 = 0xFF;
    
    // Initialize Receive Filters
    //  Filter 0 = 0x444
    //  Filter 1 = 0x111
    //  Filter 2 = 0x0A0
    RXF0EIDH = 0x00;
    RXF0EIDL = 0x00;
    RXF0SIDH = 0x88;
    RXF0SIDL = 0x80;
    RXF1EIDH = 0x00;
    RXF1EIDL = 0x00;
    RXF1SIDH = 0x22;
    RXF1SIDL = 0x20;
    RXF2EIDH = 0x00;
    RXF2EIDL = 0x00;
    RXF2SIDH = 0x14;
    RXF2SIDL = 0x00;
    
    // Enter CAN module into normal mode
    CANCON = 0x00;
    while(CANCON & 0xE0);
    
    // Set Receive Mode for buffers
    RXB0CON = 0x00;
    RXB1CON = 0x00;
    B0CON = 0x00;
    
    while(1)
        {
            //IO_RC1_Toggle();  //LED off for a short time to show we are looping
        //IO_RA0_Toggle();
        //ECAN_Transmit();
        LCDPutCmd(LCD_CLEAR);
        LCDGoto(1,0);
        LCDPutStr("CANID= ");
        
            //while(BusyXLCD());
            ECAN_Transmit();
            Delay10KTCYx(250);
            dt=ECAN_Receive();
            if(dt)
            {
                IO_RA0_Toggle();
                  Delay10KTCYx(250);
            }
            while (!dt) dt = ECAN_Receive();
            
                        
            Delay10KTCYx(250);
            
            //LCDGoto(1,1);
             int j = getTempD0(); //declare int and put 14 in it.

            char buffer[17]; //Allow 16char + one null for 16 character display
            itoa(buffer, j, 10); 
            LCDPutStr(buffer);//&tempCanMsg->frame.data0);
            
            
            //else
            //{
            //    LCDPutStr("NO");
            //}
            //LCDGoto(1,1);
            
            //LCDPutStr(read_flag);
            Delay10KTCYx(250);
            Delay10KTCYx(250);
            //uCAN_MSG txMessage;
            //CAN_transmit(&txMessage); 
            ECAN_Transmit();
            Delay10KTCYx(250);
            Delay10KTCYx(250);
            

        }  
}