/**********************************************************************
* 2010 Microchip Technology Inc.
*
* FileName:        ECAN.c
* Dependencies:    ECAN (.h) & other files if applicable, see below
* Processor:       PIC18F66K80 family
* Linker:          MPLINK 4.37+
* Compiler:        C18 3.36+
*
* 
* REVISION HISTORY:
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Author        Date      	Comments on this revision
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Manning C.    12/1/2010	First release of source file
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* Titus, Jon    5/17/2016	Comments added and text cleaned up
*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
* ADDITIONAL NOTES:
* Code Tested on: PIC18F26K80 (PIC18F66K80 family) controller (05/17/2016) 
* 
* DESCRIPTION:
* In this example, CPU is starts to run from external secondary oscillator
*  and then clock switching lets it run from internal FRC.
*********************************************************************/

/*********************************************************************
*
*                            Includes 
*
*********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <p18cxxx.h>
#include "myECAN.h"


/*********************************************************************
*
*                             Definitions 
*
*********************************************************************/
// ECAN bitrate define, choose only ONE rate
#define F_ECAN_100    1       // 1 sets ECAN module for 100kbps
#define F_ECAN_125    0       // 1 sets ECAN module for 125kbps
#define F_ECAN_500    0       // 1 sets ECAN module for 500kbps
#define F_ECAN_1000   0       // 1 sets ECAN module for 1000kbps

/*********************************************************************
*
*                            Global Variables 
*
*********************************************************************/
unsigned char temp_EIDH;    //Extended Identifier, high byte
unsigned char temp_EIDL;    //Extended Identifier, low byte
unsigned char temp_SIDH;    //Standard Identifier, high byte
unsigned char temp_SIDL;    //Standard Identifier, low byte
unsigned char temp_DLC;     //Data Length Control value, 0 to 8
unsigned char temp_D0;      //Data byte 0 through...
unsigned char temp_D1;
unsigned char temp_D2;
unsigned char temp_D3;
unsigned char temp_D4;
unsigned char temp_D5;
unsigned char temp_D6;
unsigned char temp_D7;      //Data byte 7

/*********************************************************************
*
*     Function: Check the buffers to determine if they have messages
*               if so, transfer the info to the temporary-storage
*               variables. Note: Messages to receiver 0 or 1 get saved in
*               the same variables.  This id done for simplicity in
*               this example. You could save messages to separate
*               variables, or in separate arrays, if you wish. 
*
*********************************************************************/
unsigned char ECAN_Receive(void)
{
    unsigned char RXMsgFlag;    // Temporary storage for message flag
    RXMsgFlag = 0x00;           // Set message flag to zero to start
    
    if (RXB0CONbits.RXFUL)      // Check RXB0CON bit RXFUL to see if RX Buffer 0
                                // has received a message, if so, get the
                                // associated data from the buffer and save it.
    {
        temp_EIDH = RXB0EIDH;
        temp_EIDL = RXB0EIDL;
        temp_SIDH = RXB0SIDH;
        temp_SIDL = RXB0SIDL;
        temp_DLC =  RXB0DLC;
        temp_D0 =   RXB0D0;
        temp_D1 =   RXB0D1;
        temp_D2 =   RXB0D2;
        temp_D3 =   RXB0D3;
        temp_D4 =   RXB0D4;
        temp_D5 =   RXB0D5;
        temp_D6 =   RXB0D6;
        temp_D7 =   RXB0D7;
        RXB0CONbits.RXFUL = 0;      // Reset buffer-0-full bit to show "empty"
        RXMsgFlag = 0x01;           // Set message flag to 1
    }
    else if (RXB1CONbits.RXFUL) // Check RXB1CON bit RXFUL to see if RX Buffer 1
                                // has received a message, if so, get the
                                // associated data from the buffer and save it.
    {
        temp_EIDH = RXB1EIDH;
        temp_EIDL = RXB1EIDL;
        temp_SIDH = RXB1SIDH;
        temp_SIDL = RXB1SIDL;
        temp_DLC =  RXB1DLC;
        temp_D0 =   RXB1D0;
        temp_D1 =   RXB1D1;
        temp_D2 =   RXB1D2;
        temp_D3 =   RXB1D3;
        temp_D4 =   RXB1D4;
        temp_D5 =   RXB1D5;
        temp_D6 =   RXB1D6;
        temp_D7 =   RXB1D7;
        RXB1CONbits.RXFUL = 0;      //Reset buffer-1-full bit to show "empty"
        RXMsgFlag = 0x01;           // Set message flag to 1
    }
    else if (B0CONbits.RXFUL) //CheckB0
    {
        temp_EIDH = B0EIDH;
        temp_EIDL = B0EIDL;
        temp_SIDH = B0SIDH;
        temp_SIDL = B0SIDL;
        temp_DLC = B0DLC;
        temp_D0 = B0D0;
        temp_D1 = B0D1;
        temp_D2 = B0D2;
        temp_D3 = B0D3;
        temp_D4 = B0D4;
        temp_D5 = B0D5;
        temp_D6 = B0D6;
        temp_D7 = B0D7;
        
        B0CONbits.RXFUL = 0;
        RXMsgFlag = 0x01;
    }
    
    if  (RXMsgFlag == 0x01)     // Test message flag.
                                // if message flag is a 1...
    {
        RXMsgFlag = 0x00;       // Clear the message flag
        PIR5bits.RXB1IF = 0;    // Clear the Buffer-1 interrupt bit (if used) 
        return TRUE;            // Return a "true" condition to the code that
                                // called the ECAN_Receive function.
    }
    else
    {
        return FALSE;           // Otherwise, return a false condition.
    }    
}



/*********************************************************************
*
*                      Transmit Sample Mesaage
*
*********************************************************************/
void ECAN_Transmit(void)
{
    TXB0EIDH = 0x00;
    TXB0EIDL = 0x00;
    
    //0x35E    0110 1011 110
    TXB0SIDH = 0x6B;
    TXB0SIDL = 0xC0;

    TXB0DLC = 0x03;
    TXB0D0 = 0xAA;
    TXB0D1 = 0xBB;
    TXB0D2 = 0xDD;
    
    TXB0CONbits.TXREQ = 1; //Set the buffer to transmit
    //RXB0CONbits.RXB0M0 = 1;

    
}

unsigned char getTempD0(void)
{
   return temp_D0;
}